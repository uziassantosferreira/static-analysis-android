# Summary
1. [Introduction](#introduction)
   * [Overview](#overview)   
   * [How to use](#how-to-use)  
4. [License](#license)


<a name="introduction" />

# Introduction

<a name="overview" />

## Overview
 Static Code Analysis Tools for Kotlin and Java in Android

<a name="how-to-use" />

## How to use
1. Add following lines to your build.gradle file.
2. Run `gradle check`
3. Add `check.dependsOn 'checkstyle', 'findbugs', 'pmd', 'ktlint', 'detekt'` if you want to run all tools on every `build`

 ```groovy
  apply from: '../static-analysis/findbugs.gradle'
  apply from: '../static-analysis/pmd.gradle'
  apply from: '../static-analysis/checkstyle.gradle'
  apply from: '../static-analysis/lint.gradle'
  apply from: '../static-analysis/ktlint.gradle'
  apply from: '../static-analysis/detekt.gradle'

  //Optional line
  check.dependsOn 'checkstyle', 'findbugs', 'pmd', 'ktlint', 'detekt'
```



## Example Android application with tools

<p align="center">
  <img src="example/example_1.png" align="center" width=400>
<br /><br />

</p>

